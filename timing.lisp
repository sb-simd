#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :cl-user)

(defun time-sample-form (form &optional &key (samples 10))
  (let (start end times)
    (dotimes (i samples)
      (setq start (get-internal-real-time))
      (funcall form)
      (setq end (get-internal-real-time))
      (push (- end start) times))
    
    (flet ((calc-avg (list) (float (/ (apply #'+ list) (length list))))
	   (sq (x) (* x x)))
      
    (let* ((avg (calc-avg times))
	   (sq-times (mapcar #'sq times))
	   (stddev (sqrt (- (calc-avg sq-times) (sq (calc-avg times)))))
	  )
;;      (format t "; times ~S, sqtimes ~S~%" times sq-times)
      (format t "; ~D samples, avg ~5F sec, stddev ~5F sec~%" 
	      samples 
	      (/ avg internal-time-units-per-second)
	      (/ stddev internal-time-units-per-second))))))

