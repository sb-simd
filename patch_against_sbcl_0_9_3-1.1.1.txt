diff -Naur src-093/compiler/x86/insts.lisp src/compiler/x86/insts.lisp
--- src-093/compiler/x86/insts.lisp	2005-08-05 15:31:17.723664255 +0300
+++ src/compiler/x86/insts.lisp	2005-08-05 15:42:36.536109257 +0300
@@ -192,6 +192,7 @@
     (:byte 8)
     (:word 16)
     (:dword 32)
+    (:dqword 128)
     (:float 32)
     (:double 64)))
 
@@ -671,7 +672,7 @@
 
 (defun reg-tn-encoding (tn)
   (declare (type tn tn))
-  (aver (eq (sb-name (sc-sb (tn-sc tn))) 'registers))
+;  (aver (eq (sb-name (sc-sb (tn-sc tn))) 'registers))
   (let ((offset (tn-offset tn)))
     (logior (ash (logand offset 1) 2)
             (ash offset -1))))
@@ -718,6 +719,8 @@
      (ecase (sb-name (sc-sb (tn-sc thing)))
        (registers
         (emit-mod-reg-r/m-byte segment #b11 reg (reg-tn-encoding thing)))
+       (sse-registers
+        (emit-mod-reg-r/m-byte segment #b11 reg (reg-tn-encoding thing)))
        (stack
         ;; Convert stack tns into an index off of EBP.
         (let ((disp (- (* (1+ (tn-offset thing)) n-word-bytes))))
@@ -830,6 +833,10 @@
   (and (tn-p thing)
        (eq (sb-name (sc-sb (tn-sc thing))) 'registers)))
 
+(defun sse-register-p (thing)
+  (and (tn-p thing)
+       (eq (sb-name (sc-sb (tn-sc thing))) 'sse-registers)))
+
 (defun accumulator-p (thing)
   (and (register-p thing)
        (= (tn-offset thing) 0)))
@@ -2042,6 +2049,123 @@
   (:emitter
    (emit-header-data segment return-pc-header-widetag)))
 
+
+;;;; SSE instructions
+;;;; 
+;;;; Automatically generated
+
+
+(DEFINE-INSTRUCTION ADDPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 88)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION ADDSUBPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 242)
+                              (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 208)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION ANDNPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 85)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION ANDPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 84)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION DIVPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 94)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION MAXPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 95)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION MINPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 93)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION MULPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 89)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION ORPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 86)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION RCPPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 83)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION RSQRTPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 82)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION SQRTPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 81)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION SUBPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 92)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+(DEFINE-INSTRUCTION XORPS
+                    (SEGMENT DST SRC)
+                    (:EMITTER (EMIT-BYTE SEGMENT 15)
+                              (EMIT-BYTE SEGMENT 87)
+                              (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST))))
+
+;;; SSE MOVE
+
+(DEFINE-INSTRUCTION MOVUPS (SEGMENT DST SRC)
+  (:EMITTER
+   (COND
+     ((SSE-REGISTER-P DST) 
+      (EMIT-BYTE SEGMENT 15)
+      (EMIT-BYTE SEGMENT 16)
+      (EMIT-EA SEGMENT SRC (REG-TN-ENCODING DST)))
+     (T (EMIT-BYTE SEGMENT 15)
+	(EMIT-BYTE SEGMENT 17)
+	(EMIT-EA SEGMENT DST (REG-TN-ENCODING SRC))))))
+
+
+;;; CPUID
+
+
+(define-instruction cpuid (segment)
+  (:emitter
+   (emit-byte segment #x0F)
+   (emit-byte segment #xA2)))
+
+    
+
+
+
 ;;;; fp instructions
 ;;;;
 ;;;; FIXME: This section said "added by jrd", which should end up in CREDITS.
diff -Naur src-093/compiler/x86/vm.lisp src/compiler/x86/vm.lisp
--- src-093/compiler/x86/vm.lisp	2005-08-05 15:32:19.810183044 +0300
+++ src/compiler/x86/vm.lisp	2005-08-05 15:38:26.784310770 +0300
@@ -21,7 +21,8 @@
   (defvar *byte-register-names* (make-array 8 :initial-element nil))
   (defvar *word-register-names* (make-array 16 :initial-element nil))
   (defvar *dword-register-names* (make-array 16 :initial-element nil))
-  (defvar *float-register-names* (make-array 8 :initial-element nil)))
+  (defvar *float-register-names* (make-array 8 :initial-element nil))
+  (defvar *dqword-register-names* (make-array 8 :initial-element nil)))
 
 (macrolet ((defreg (name offset size)
              (let ((offset-sym (symbolicate name "-OFFSET"))
@@ -91,6 +92,17 @@
   (defreg fr7 7 :float)
   (defregset *float-regs* fr0 fr1 fr2 fr3 fr4 fr5 fr6 fr7)
 
+  ;; sse registers
+  (defreg xmm0 0 :dqword)
+  (defreg xmm1 1 :dqword)
+  (defreg xmm2 2 :dqword)
+  (defreg xmm3 3 :dqword)
+  (defreg xmm4 4 :dqword)
+  (defreg xmm5 5 :dqword)
+  (defreg xmm6 6 :dqword)
+  (defreg xmm7 7 :dqword)
+  (defregset *sse-regs* xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6 xmm7)
+  
   ;; registers used to pass arguments
   ;;
   ;; the number of arguments/return values passed in registers
@@ -118,6 +130,8 @@
 ;;; the new way:
 (define-storage-base float-registers :finite :size 8)
 
+(define-storage-base sse-registers :finite :size 8)
+
 (define-storage-base stack :unbounded :size 8)
 (define-storage-base constant :non-packed)
 (define-storage-base immediate-constant :non-packed)
@@ -320,6 +334,8 @@
                     :save-p t
                     :alternate-scs (complex-long-stack))
 
+  (sse-reg sse-registers
+	   :locations #.*sse-regs*)
   ;; a catch or unwind block
   (catch-block stack :element-size kludge-nondeterministic-catch-block-size))
 
@@ -337,6 +353,7 @@
 ;;; These are used to (at least) determine operand size.
 (defparameter *float-sc-names* '(single-reg))
 (defparameter *double-sc-names* '(double-reg double-stack))
+(defparameter *dqword-sc-names* '(sse-reg))
 ) ; EVAL-WHEN
 
 ;;;; miscellaneous TNs for the various registers
@@ -444,6 +461,7 @@
              ;; FIXME: Shouldn't this be an ERROR?
              (format nil "<unknown reg: off=~W, sc=~A>" offset sc-name))))
       (float-registers (format nil "FR~D" offset))
+      (sse-registers (format nil "XMM~D" offset))
       (stack (format nil "S~D" offset))
       (constant (format nil "Const~D" offset))
       (immediate-constant "Immed")
