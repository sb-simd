#!/bin/sh
find . -name '.emacs*' |xargs rm  2>/dev/null
find . -name '*~' |xargs rm 2>/dev/null
diff -x "CVS*" -Naur src-093 src > patch_against_sbcl_0_9_3
