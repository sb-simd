(in-package :sb-vm)

(defmacro vect-ea (base &optional idx (width :dword)) 
  (let ((disp
	 (if (and idx (numberp idx))
	     `(+ (- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG) ,idx)
	     `(- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG))))

;;    (format t "ea ~A ~A ~A~%" base idx (and idx (symbolp idx)))
    (if (and idx (symbolp idx))
	`(make-ea ,width :base ,base :index ,idx :disp ,disp)
	`(make-ea ,width :base ,base :disp ,disp))))


(DEFINE-VOP (%sse-seq=)
    (:POLICY :FAST-SAFE)
  (:ARGS (seq1 :SCS (DESCRIPTOR-REG))
	 (seq2 :SCS (DESCRIPTOR-REG)))
  (:ARG-TYPES simple-array-unsigned-byte-8 simple-array-unsigned-byte-8 )

  (:results (RESULT :SCS (DESCRIPTOR-REG)))

  (:result-types fixnum)

  (:TEMPORARY (:SC XMM-REG) X0)
  (:TEMPORARY (:SC XMM-REG) X1)
  (:TEMPORARY (:SC XMM-REG) X2)
  (:TEMPORARY (:SC XMM-REG) X3)
  (:TEMPORARY (:SC XMM-REG) X4)
  (:TEMPORARY (:SC XMM-REG) X5)

  (:TEMPORARY (:SC unsigned-reg :offset eax-offset :to (:result 0)) temp1)
  (:TEMPORARY (:SC unsigned-reg :offset edx-offset) temp2)
  (:TEMPORARY (:SC unsigned-reg :offset ebx-offset) index)
  (:TEMPORARY (:SC unsigned-reg :offset ecx-offset) length)

  (:GENERATOR 10

    (let ((top (gen-label))
	  (top2 (gen-label))
	  (length-ok (gen-label))
	  (fail (gen-label))
	  (the-end (gen-label))
	  (end (gen-label)))

	      (loadw index seq1 vector-length-slot other-pointer-lowtag)
	      (loadw length seq2 vector-length-slot other-pointer-lowtag)

	      ;; same length ?
	      (inst cmp index length)
	      (inst jmp :eq length-ok)

	      ;; not same length, fail
	      (inst jmp fail)

	      (emit-label length-ok)

	      ;; un-fixnumize length
	      (inst shr length 2)

	      ;; calc number of 256bit blocks
	      (inst shr length (floor (log (/ 256 8) 2)))

	      ;; init indices
	      (inst xor index index)

	      ;; zero eq-regs
;;	      (inst pxor x4 x4)
;;	      (inst pxor x5 x5)

	      (emit-label top)

	      ;; load first blocks
	      (inst movdqu x0 (vect-ea seq1 index :xmmword))
	      (inst movdqu x1 (vect-ea seq2 index :xmmword))

	      (inst pxor x4 x4)
	      (inst pxor x5 x5)

	      ;; load second blocks
	      (inst movdqu x2 
		    (make-ea :xmmword :base seq1 :index index 
			     :disp (+ (- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG) 16)))
	      (inst movdqu x3 
		    (make-ea :xmmword :base seq2 :index index 
			     :disp (+ (- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG) 16)))


	      ;; xor first/second blocks (i.e. if equal, xor will be zero)
	      (inst pxor x0 x1)
	      (inst pxor x2 x3)

	      ;; add index
	      (inst add index 32)

	      ;; check for non-equality
	      (inst pcmpeqd x4 x0)
	      (inst pcmpeqd x5 x2)

	      (inst pmovmskb temp1 x4)
	      (inst pmovmskb temp2 x5)

	      (inst cmp temp1 #x0000FFFF)
	      (inst jmp :ne fail)

	      (inst cmp temp2 #x0000FFFF)
	      (inst jmp :ne fail)

	      ;; loop
	      (inst dec length)
	      (inst jmp :nz top)


	      ;; all 256bit blocks done


	      ;; check remaining bytes
	      (loadw length seq1 vector-length-slot other-pointer-lowtag)
	      (inst shr length 2)
 	      (inst and length (1- (/ 256 8)))

	      ;; no bytes left ?
	      (inst test length length)
	      (inst jmp :z end)

	      (inst xor temp1 temp1)
	      (inst xor temp2 temp2)

	      (emit-label top2)

	      ;; test bytes
	      (inst movzx temp1 (vect-ea seq1 index :byte))
	      (inst movzx temp2 (vect-ea seq2 index :byte))
	      (inst xor temp1 temp2)
	      (inst inc index)

	      ;; if not zero, fail
	      (inst test temp1 temp1)
	      (inst jmp :nz fail)

	      ;; loop
	      (inst dec length)
	      (inst jmp :nz top2)

	      ;; end
	      (emit-label end)

	      (inst mov result (fixnumize 0))
	      (inst jmp the-end)

              ;; fail
	      (emit-label fail)
	      (inst mov result (fixnumize 1))

	      ;; the-end
	      (emit-label the-end)

	      )))
