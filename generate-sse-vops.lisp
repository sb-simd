#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#

(defun vect-ea (vect &optional (idx nil))
  `(make-ea :dword :base ,vect ,@(if idx `(:index ,idx))
    :disp (- (* vector-data-offset n-word-bytes) other-pointer-lowtag)))

(defun gen-vops-to-file (filename)
  (with-open-file (stream filename :direction :output :if-exists :supersede)
    (gen-vops stream)))

(defun gen-vops (&optional (stream t))

  (format stream "(in-package :sb-vm)~%~%")

  ;; TWO-ARG SSE VOPs
  (loop for (op-name type mov-inst op-inst elem-width) in
	'(
	  ;; single float
	  (add    single-float movups addps    4)
	  (addsub single-float movups addsubps 4)
;;	  (andnot single-float movups andnps   4)
;;	  (and    single-float movups andps    4)
	  (div    single-float movups divps    4)
	  (hadd   single-float movups haddps   4)
	  (hsub   single-float movups hsubps   4)
	  (max    single-float movups maxps    4)
	  (min    single-float movups minps    4)
	  (mul    single-float movups mulps    4)
;;	  (or     single-float movups orps     4)
	  (sub    single-float movups subps    4)
;;	  (xor    single-float movups xorps    4)

	  ;; double float
	  (add    double-float movupd addpd    8)
	  (addsub double-float movupd addsubpd 8)
;;	  (andnot double-float movupd andnpd   8)
;;	  (and    double-float movupd andpd    8)
	  (div    double-float movupd divpd    8)
	  (hadd   double-float movupd haddpd   8)
	  (hsub   double-float movupd hsubpd   8)
	  (max    double-float movupd maxpd    8)
	  (min    double-float movupd minpd    8)
	  (mul    double-float movupd mulpd    8)
;;	  (or     double-float movupd orpd     8)
	  (sub    double-float movupd subpd    8)
;;	  (xor    double-float movupd xorpd    8)

	  ;; unsigned byte 8
	  (add    unsigned-byte-8 movdqu paddb  1)
	  (avg    unsigned-byte-8 movdqu pavgb  1)
	  (max    unsigned-byte-8 movdqu pmaxub 1)
	  (min    unsigned-byte-8 movdqu pminub 1)
	  (sub    unsigned-byte-8 movdqu psubb  1)

	  (and    unsigned-byte-8 movdqu pand   1)
	  (andn   unsigned-byte-8 movdqu pandn  1)
	  (or     unsigned-byte-8 movdqu por    1)
	  (xor    unsigned-byte-8 movdqu pxor   1)

	  ;; unsigned byte 16
	  (add    unsigned-byte-16 movdqu paddw 2)
	  (avg    unsigned-byte-16 movdqu pavgw 2)
	  (sub    unsigned-byte-16 movdqu psubw 2)

	  (and    unsigned-byte-16 movdqu pand  2)
	  (andn   unsigned-byte-16 movdqu pandn 2)
	  (or     unsigned-byte-16 movdqu por   2)
	  (xor    unsigned-byte-16 movdqu pxor  2)

	  (shl    unsigned-byte-16 movdqu psllw 2)
	  (shr    unsigned-byte-16 movdqu psrlw 2)

	  ;; signed byte 16
	  (add    signed-byte-16 movdqu paddw   2)
	  (max    signed-byte-16 movdqu pmaxsw  2)
	  (min    signed-byte-16 movdqu pminsw  2)
	  (sub    signed-byte-16 movdqu psubw   2)

	  (and    signed-byte-16 movdqu pand    2)
	  (andn   signed-byte-16 movdqu pandn   2)
	  (or     signed-byte-16 movdqu por     2)
	  (xor    signed-byte-16 movdqu pxor    2)

	  (shl    signed-byte-16 movdqu psllw   2)
	  (shr    signed-byte-16 movdqu psraw   2)
	  )
	do

	(format stream "~S~%~%"      
		`(define-vop (,(intern (let ((name (format nil "%SSE-~A/SIMPLE-ARRAY-~A-1" op-name type)))
					 (format t "; defining VOP ~A..~%" name)
					 name)))

		  (:policy :fast-safe)

		  ;;(:guard (member :sse2 *backend-subfeatures*))

		  (:args 
		   (result :scs (descriptor-reg))
		   (vect1 :scs (descriptor-reg))
		   (vect2 :scs (descriptor-reg))
		   (index :scs (unsigned-reg)))

		  (:arg-types 
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type))
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type))
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type))
		   fixnum)

		  (:temporary (:sc xmm-reg) sse-temp1)
		  (:temporary (:sc xmm-reg) sse-temp2)

		  (:generator 10

		   ;; scale index by 4 (size-of single-float)
		   (inst shl index ,(floor (log elem-width 2)))

		   ;; load
		   (inst ,mov-inst sse-temp1 ,(vect-ea 'vect1 'index))
		   (inst ,mov-inst sse-temp2 ,(vect-ea 'vect2 'index))

		   ;; operate
		   (inst ,op-inst sse-temp1 sse-temp2)

		   ;; store
		   (inst ,mov-inst ,(vect-ea 'result 'index) sse-temp1)
		   ))))

  ;; TWO-ARG SSE VOPs w/ DIFFERENT ARG TYPES
  (loop for (op-name type1 type2 mov-inst1 mov-inst2 op-inst elem-width) in
	'(
	  (andnot single-float unsigned-byte-8 movups movdqu andnps   4)
	  (and    single-float unsigned-byte-8 movups movdqu andps    4)
	  (or     single-float unsigned-byte-8 movups movdqu orps     4)
	  (xor    single-float unsigned-byte-8 movups movdqu xorps    4)

	  (andnot double-float unsigned-byte-8 movupd movdqu andnpd   4)
	  (and    double-float unsigned-byte-8 movupd movdqu andpd    4)
	  (or     double-float unsigned-byte-8 movupd movdqu orpd     4)
	  (xor    double-float unsigned-byte-8 movupd movdqu xorpd    4)
	  )
	do
	(format stream "~S~%~%"      
		`(define-vop (,(intern (let ((name (format nil "%SSE-~A/SIMPLE-ARRAY-~A/SIMPLE-ARRAY-~A-1" op-name type1 type2)))
					 (format t "; defining VOP ~A..~%" name)
					 name)))

		  (:policy :fast-safe)

		  ;;(:guard (member :sse2 *backend-subfeatures*))

		  (:args 
		   (result :scs (descriptor-reg))
		   (vect1 :scs (descriptor-reg))
		   (vect2 :scs (descriptor-reg))
		   (index :scs (unsigned-reg)))

		  (:arg-types 
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type2))
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type1))
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type2))
		   fixnum)

		  (:temporary (:sc xmm-reg) sse-temp1)
		  (:temporary (:sc xmm-reg) sse-temp2)

		  (:generator 10

		   ;; scale index by 4 (size-of single-float)
		   (inst shl index ,(floor (log elem-width 2)))

		   ;; load
		   (inst ,mov-inst1 sse-temp1 ,(vect-ea 'vect1 'index))
		   (inst ,mov-inst2 sse-temp2 ,(vect-ea 'vect2))

		   ;; operate
		   (inst ,op-inst sse-temp1 sse-temp2)

		   ;; store
		   (inst ,mov-inst2 ,(vect-ea 'result 'index) sse-temp1)
		   ))))
	

  ;; SINGLE-ARG SSE VOPs
  (loop for (op-name type mov-inst op-inst elem-width) in
	'(
	  (recip  single-float movups rcpps   4)
	  (rsqrt  single-float movups rsqrtps 4)
	  (sqrt   single-float movups sqrtps  4)
	  (sqrt   double-float movupd sqrtpd  8)
	  )
	do
      	(format stream "~S~%~%"
		`(define-vop (,(intern (let ((name (format nil "%SSE-~A/SIMPLE-ARRAY-~A-1" op-name type)))
					 (format t "; defining VOP ~A..~%" name)
					 name)))
		  (:policy :fast-safe)

		  ;;(:guard (member :sse2 *backend-subfeatures*))

		  (:args 
		   (result :scs (descriptor-reg))
		   (vect1 :scs (descriptor-reg))
		   (index :scs (unsigned-reg)))

		  (:arg-types 
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type))
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type))
		   fixnum)

		  (:temporary (:sc xmm-reg) sse-temp1)
		  (:temporary (:sc xmm-reg) sse-temp2)

		  (:generator 10

		   ;; scale index by 4 (size-of single-float)
		   (inst shl index ,(floor (log elem-width 2)))

		   ;; load
		   (inst ,mov-inst sse-temp1 ,(vect-ea 'vect1 'index))

		   ;; operate
		   (inst ,op-inst sse-temp2 sse-temp1)

		   ;; store
		   (inst ,mov-inst ,(vect-ea 'result 'index) sse-temp2)
		   ))))

  ;; COMPARE
  (loop for (op-name type mov-inst op-inst elem-width) in
	'(
	  (cmp    single-float movups cmpps 4)
	  (cmp    double-float movupd cmppd 8)
	  )
	do
	(format stream "~S~%~%"      
		`(define-vop (,(intern (let ((name (format nil "%SSE-~A/SIMPLE-ARRAY-~A-1" op-name type)))
					 (format t "; defining VOP ~A..~%" name)
					 name)))

		  (:policy :fast-safe)

		  ;;(:guard (member :sse2 *backend-subfeatures*))

		  (:args 
		   (result :scs (descriptor-reg))
		   (vect1 :scs (descriptor-reg))
		   (vect2 :scs (descriptor-reg))
		   (index :scs (unsigned-reg)))

		  (:info cond)

		  (:arg-types 
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type))
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type))
		   ,(intern (format nil "SIMPLE-ARRAY-~A" type))
		   fixnum
		   (:constant keyword)
		   )

		  (:temporary (:sc xmm-reg) sse-temp1)
		  (:temporary (:sc xmm-reg) sse-temp2)

		  (:generator 10

		   ;; scale index by 4 (size-of single-float)
		   (inst shl index ,(floor (log elem-width 2)))

		   ;; load
		   (inst ,mov-inst sse-temp1 ,(vect-ea 'vect1 'index))
		   (inst ,mov-inst sse-temp2 ,(vect-ea 'vect2 'index))

		   ;; operate
		   (inst ,op-inst sse-temp1 sse-temp2 cond)

		   ;; store
		   (inst ,mov-inst ,(vect-ea 'result 'index) sse-temp1)
		   ))))

  )
