(if nil
    (progn 
      (load (compile-file "sse-vops.lisp"))
      (load (compile-file "example-test.lisp"))
      ))

(if nil
    (progn
      (load (compile-file "sse-matrix.lisp"))
      (load (compile-file "timing.lisp"))
      (load (compile-file "test-matrix.lisp"))
      ))
      
(if nil
    (progn
      (load (compile-file "detect-simd.lisp"))
      (load (compile-file "expand-parse-operand-temp-count.lisp"))
      (load (compile-file "timing.lisp"))
      (load (compile-file "sse-seq.lisp"))
      (load (compile-file "test-seq.lisp"))
      ))
      
(if t
    (progn
      (load (compile-file "detect-simd.lisp"))
      (load (compile-file "sse-moves.lisp"))
      (load (compile-file "expand-parse-operand-temp-count.lisp"))
      (load (compile-file "timing.lisp"))
      (load (compile-file "sse-vector.lisp"))
      (load (compile-file "test-vector.lisp"))
      ))
      